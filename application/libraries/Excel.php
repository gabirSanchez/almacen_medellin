<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."/third_party/PHPExcel.php";

class Excel extends PHPExcel {

  private $phpexcel;
  private $nombre;

  public function __construct() {
    parent::__construct();
    $this->phpexcel = new PHPExcel();
    $this->phpexcel->getProperties()
      ->setCreator("gaCodes")
			->setLastModifiedBy("gaCodes")
			->setTitle("Reporte Aplicación Inventarios")
			// ->setSubject("Usuarios AD Mesa de Ayuda")
			->setDescription("Archivo generado automaticamente por gaCodes para Niñolandia")
			->setKeywords("gaCodes, PHPExcel")
			->setCategory("");
  }

  public function descargar() {
      // Redirect output to a client’s web browser (Excel2007)
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Disposition: attachment;filename="'.$this->nombre.'.xlsx"');
      header('Cache-Control: max-age=0');
      // If you're serving to IE 9, then the following may be needed
      header('Cache-Control: max-age=1');

      // If you're serving to IE over SSL, then the following may be needed
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0
      
      $objWriter = PHPExcel_IOFactory::createWriter($this->phpexcel, 'Excel5');
      $objWriter->save('php://output');
      
    exit;
  }

  public function crearExcel($nombre, $cabeceras, $datos){
    $this->nombre = $nombre;
    //print_r($cabeceras);die();
    $this->insertarCabeceras($cabeceras);
    $this->insertarDatos( $datos );
   
    $this->phpexcel->getActiveSheet()->setTitle('Inventario '. date('Ymd'));
    $this->phpexcel->setActiveSheetIndex(0);

    $this->descargar();
  }

  private function insertarCabeceras($cabeceras){
  		$formatoTitulo = $this->darFormato();
			foreach ($cabeceras as $key => $value) {
        $LETRA = $this->letra($key);
        $this->phpexcel->getActiveSheet()
            ->setCellValue($LETRA.'1', strtoupper($value))
            ->getStyle($LETRA.'1')
            ->applyFromArray($formatoTitulo);
        $this->phpexcel->getActiveSheet()
            ->getColumnDimension($LETRA)->setWidth(18);
			}
  	}

    private function insertarDatos($datos){
      
    		$formatoTitulo = $this->darFormato();
        foreach ( $datos as $key => $valores) {
          
          $indiceLetra = 0;
          foreach ( $valores as $kayA => $value) {
            $LETRA = $this->letra($indiceLetra);
            
            $this->phpexcel->getActiveSheet()->setCellValue($LETRA.($key+2), $value);
            $this->phpexcel->getActiveSheet()->getColumnDimension($LETRA)->setWidth(18);
            $indiceLetra++;
          }
        }
    	}

  	private function darFormato(){
      $formatoEtiqueta = array(
          'font' => array(
            'bold' => true,
            'color' => array(
                'rgb' => 'FFFFFF',
            ),
          ),
          'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'wrap' => TRUE,
          ),
          'borders' => array(
              'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN,),
          ),
          'fill' => array(
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array(
                  'rgb' => '00233C',// 'E31818',
              ),
              // 'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
              // 'rotation' => 90,
              // 'startcolor' => array('argb' => 'FFA0A0A0',),
              // 'endcolor' => array('argb' => 'FFFFFFFF',),
          ),
      );
  		return $formatoEtiqueta;
  	}

    private function letra($columna){
  		static $letrasColumnas = array(
        'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
        'AA','AB','AC','AD','AE', 'AF', 'AG', 'AH', 'AI','AJ', 'AK'
      );
  		return $letrasColumnas[$columna];
  	}

}

?>
